<!--
SPDX-FileCopyrightText: 2024 Guilherme Leoi

SPDX-License-Identifier: CC-BY-4.0
-->

# Tiny Genetic Programming in Julia

[![Code licensed under GPL version 3.0 only](https://img.shields.io/badge/Code_License-GPL--3.0--only-blue)](./LICENSES/GPL-3.0-only.txt)
[![Etc. licensed under Creative Commons Attribution 4.0 International](https://img.shields.io/badge/Etc._License-CC--BY--4.0-purple)](./LICENSES/CC-BY-4.0.txt)
[![REUSE status](https://api.reuse.software/badge/codeberg.org/leoi/TinyGP.jl)](https://api.reuse.software/info/codeberg.org/leoi/TinyGP.jl)

A minimalistic program implementing Koza-style (tree-based) genetic programming to solve a symbolic regression problem.

Inspired by https://github.com/moshesipper/tiny_gp, with some different decisions (e.g. use of functions without side effects).

## Usage
`julia TinyGP.jl [generate-plot] [quiet]`

`julia TinyGPExtra.jl [generate-plot] [print-expression] [quiet] <path/to/file.csv>`

`julia MvSR.jl [generate-plot] [print-expression] [quiet] <path/to/directory>`

`julia Batch.jl base|extra|mvsr|base_fresh|extra_fresh|mvsr_fresh|python [retries:n] <path/to/target>`

## Installation
`julia Requirements.jl`

*Tested on Julia version 1.6.7 (LTS)*

## Performance
See [BENCHMARK.md](./BENCHMARK.md)

## Results
### [TinyGP.jl](./TinyGP.jl)

| GP | Description |
|---:|:------------| 
| Objective | Find an expression with one input (independent variable x), whose output equals the value of the quartic function $x^4 + x^3 + x^2 + x + 1$ |
| Function set | `+`, `-`, `*` |   
| Terminal set | `x`, `-2`, `-1`, `0`, `1`, `2`  |   
| Fitness | Inverse mean absolute error over a dataset of 101 target values, normalized to $[0,1]$
| Paremeters | pop_size (population size), min_depth (minimal initial random tree depth), max_depth (maximal initial random tree depth), generations (maximal number of generations), tournament_size (size of tournament for tournament selection), crossover_rate (per-tree crossover rate), mutation_rate (per-node mutation rate) |
| Termination | Maximal number of generations reached |

<br/>

![Without bloat control](./Figures/without-bloat-control.png)
<br/>
*Interactive version: [without-bloat-control.html](https://geeleonidas.codeberg.page/TinyGP.jl/without-bloat-control.html)*

Best of run: `((1 * ((((x * ((((0 * x) + (2 + x)) * x) * x)) - -1) + ((x + (1 + -2)) * ((((((x + (1 + -2)) * (-1 * 0)) * ((x + (x * x)) * x)) - ((x * (2 + x)) - -1)) + (0 * x)) * x))) * x)) - -1)`

<br/>

![With bloat control](./Figures/with-bloat-control.png)
<br/>
*Interactive version: [with-bloat-control.html](https://geeleonidas.codeberg.page/TinyGP.jl/with-bloat-control.html)*

Best of run: `(((1 + ((1 + ((1 + x) * x)) * x)) * x) + 1)`

### [TinyGPExtra.jl](./TinyGPExtra.jl)

| GP | Description |
|---:|:------------| 
| Objective | Find an expression with one input (independent variable x), whose output equals the value in the last column (target) of a tabular file |
| Function set (binary) | `+`, `-`, `*`, `/`, `pow_abs` |   
| Function set (unary) | `inv` |
| Terminal set | `x`, `OptimParam` |   
| Fitness | Inverse mean squared error over a dataset of 362 target values, normalized to $[0,1]$
| Paremeters | pop_size (population size), min_depth (minimal initial random tree depth), max_depth (maximal initial random tree depth), generations (maximal number of generations), tournament_size (size of tournament for tournament selection), crossover_rate (per-tree crossover rate), mutation_rate (per-node mutation rate), max_size (hard-limit on tree size) |
| Termination | Maximal number of generations reached |

### [MvSR.jl](./MvSR.jl)

| GP | Description |
|---:|:------------| 
| Objective | Find an expression with one input (independent variable x), whose output equals the value in the last column (target) of various tabular files inside a directory |
| Function set (binary) | `+`, `-`, `*`, `/` |   
| Function set (unary) | `squared`, `sqrt_abs`, `exp` |
| Terminal set | `x`, `OptimParam` |   
| Fitness | Worst inverse mean squared error over various datasets, normalized to $[0,1]$
| Paremeters | pop_size (population size), min_depth (minimal initial random tree depth), max_depth (maximal initial random tree depth), generations (maximal number of generations), tournament_size (size of tournament for tournament selection), crossover_rate (per-tree crossover rate), mutation_rate (per-node mutation rate), max_size (hard-limit on tree size), aggregation_func (function that aggregates dataset errors), param_penalty_weight (regulates parameter bloat control), pool_size (offspring number per generation) |
| Termination | Maximal number of generations reached |