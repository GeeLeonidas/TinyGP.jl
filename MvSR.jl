# SPDX-FileCopyrightText: 2019 Moshe Sipper, www.moshesipper.com
# SPDX-FileCopyrightText: 2024 Fabrício Olivetti de França, folivetti.github.io
# SPDX-FileCopyrightText: 2024 Guilherme Leoi
#
# SPDX-License-Identifier: GPL-3.0-only

# Changes related to tiny_gp:
# - Implementation of various functions without side-effects
# - Mutation can occur on both branches, not only on left branches
# - Terminals are stored as values, not as childless tree nodes
# - Usage of PlotlyJS for plotting
# - Support for unary functions
# - Dataset reading from multiple CSV files inside a folder
# - Conversion from tree to simplified expression
# - Parameter optimization per dataset
# - Tree size hard-limit as bloat control
# - Aggregation of dataset errors
# - Parameter count penalty as bloat control
# - Mean R² measurement
# - Specify how many individuals are created/replaced each generation
# - All individuals from the first generation are valid

struct OptimParam
    val_vec_ref::Ref

    OptimParam() = new(Ref(Vector{Float64}()))
end

log_abs(x) = log(abs(x))
sqrt_abs(x) = @fastmath sqrt(abs(x))
pow_abs(x, y) = @fastmath (abs(x)^y)
squared(x) = x^2

mutable struct GPHyperparameters
    binary_functions # Available two-parameter functions for an expression tree (default: [+, -, *, /])
    unary_functions # Available one-parameter functions for an expression tree (default: [squared, sqrt_abs, exp])
    terminals # Available leaf nodes for an expression tree (default: [:x, OptimParam])
    pop_size # Number of individuals in the population (default: 1000)
    min_depth # Minimum depth for an expression tree (default: 2)
    max_depth # Maximum depth for an expression tree (default: 5)
    tournament_size # Number of combatants in each round (default: 10)
    crossover_rate # Per-tree rate for crossover (default: 1.0)
    mutation_rate # Per-tree rate for mutation (default: 0.25)
    generations # Number of iterations over the population (default: 1000)
    max_size # Maximum node count for an expression tree, acts as bloat control (default: 12)
    aggregation_func # Aggregation function for the overall error (default: maximum)
    param_penalty_weight # Penalty weight for parameter bloat control (default: 10^-3)
    pool_size # Number of offspring created per generation (default: 5)

    GPHyperparameters() = new([+, -, *, /], [squared, sqrt_abs, exp], [:x, OptimParam], 1000, 2, 5, 10, 1.0, 0.25, 1000, 12, maximum, 10^-3, 5)
end

struct GPTree
    func
    left
    right
end

function string_gptree(node)::String
    if !(node isa GPTree)
        return node isa OptimParam ? string("param", node.val_vec_ref[]) : string(node)
    end
    l = string_gptree(node.left)
    if (isnothing(node.right))
        return string('(', node.func, ' ', l, ')')
    else
        r = string_gptree(node.right)
        return string('(', l, ' ', node.func, ' ', r, ')')
    end
end

function string_gptree_pretty(node, terminals::Vector)::String
    symbols = filter(item -> item isa Symbol && item != :p, terminals)
    symbols_params = [Symbol("p", idx) for idx = 1:countgptree(node, (x) -> x isa OptimParam)]
    u = eval(:(@syms $(symbols...)))
    u_params = eval(:(@syms $(symbols_params...)))
    symbols_to_u = Dict([(symbols[idx], u[idx]) for idx = eachindex(symbols)])
    expr = evalgptree(node, symbols_to_u, [pn for pn = u_params])
    try
        expr_simplified = simplify(expr, expand=true)
        return string(expr_simplified)
    finally
        return string(expr)
    end
end
string_gptree_pretty(node, hyper::GPHyperparameters)::String =
    string_gptree_pretty(node, hyper.terminals)

function evalgptree(node, data::Dict, params::Vector, param_idx_ref::Ref)
    if node isa GPTree
        try
            l = evalgptree(node.left, data, params, param_idx_ref)
            if (isnothing(node.right))
                return node.func(l)
            else
                r = evalgptree(node.right, data, params, param_idx_ref) 
                return node.func(l, r)
            end
        catch e
            if e isa DomainError || e isa DivideError
                return NaN
            else
                throw(e)
            end
        end
    elseif node isa OptimParam
        param = params[param_idx_ref[]]
        param_idx_ref[] += 1
        return param
    elseif node in keys(data)
        return data[node]
    end

    return node
end
evalgptree(node, data::Dict, params::Vector) =
    evalgptree(node, data, params, Ref(1))

function randomgptree(binary_functions::Vector, unary_functions::Vector,
                      terminals::Vector, min_depth::Integer, max_depth::Integer,
                      max_size::Integer, count::Integer, grow::Bool = true, depth::Integer = 0)
    functions = vcat(binary_functions, unary_functions)
    max_arity = (isempty(binary_functions) ? 1 : 2)
    pre_last_leaves = max_size - max_arity
    g = if (depth < min_depth || (depth < max_depth && !grow)) && count <= pre_last_leaves
        rand(functions)
    elseif depth >= max_depth || count > pre_last_leaves
        rand(terminals)
    else
        rand() < 0.5 ? rand(functions) : rand(terminals)
    end

    if !(g isa Function)
        return g isa Type ? g() : g
    end

    g_extra = g in unary_functions ? 0 : 1
    l = randomgptree(binary_functions, unary_functions, terminals, min_depth, max_depth, max_size, count + g_extra + 1, grow, depth + 1)
    r = nothing
    if g_extra > 0
        l_count = countgptree(l)
        r = randomgptree(binary_functions, unary_functions, terminals, min_depth, max_depth, max_size, count + l_count + 1, grow, depth + 1)
    end

    return GPTree(g, l, r)
end
randomgptree(hyper::GPHyperparameters, grow::Bool = true, depth::Integer = 0) =
    randomgptree(hyper.binary_functions, hyper.unary_functions, hyper.terminals, hyper.min_depth, hyper.max_depth, hyper.max_size, 1, grow, depth)

function mutategptree(node, binary_functions::Vector, unary_functions::Vector,
                      terminals::Vector, min_depth::Integer, mutation_rate::AbstractFloat,
                      max_size::Integer, depth::Integer, index_ref::Ref, tree_size::Integer = countgptree(node))
    if rand() < mutation_rate || depth > 0
        index_ref[] -= 1

        if index_ref[] == 0
            adapted_max_size = max_size - tree_size + countgptree(node)
            return randomgptree(binary_functions, unary_functions, terminals, min_depth, 2, adapted_max_size, 1, true, depth)
        elseif !(node isa GPTree) || index_ref[] < 0
            return node
        end

        l = mutategptree(node.left, binary_functions, unary_functions, terminals, min_depth, mutation_rate, max_size, depth + 1, index_ref, tree_size)
        r = isnothing(node.right) ?
            nothing :
            mutategptree(node.right, binary_functions, unary_functions, terminals, min_depth, mutation_rate, max_size, depth + 1, index_ref, tree_size)

        return GPTree(node.func, l, r)
    end

    return node
end
mutategptree(node, hyper::GPHyperparameters, at_idx::Integer) =
    mutategptree(node, hyper.binary_functions, hyper.unary_functions, hyper.terminals, hyper.min_depth, hyper.mutation_rate, hyper.max_size, 0, Ref(at_idx))

function flattengptree(node, filter = nothing)
    if !(node isa GPTree)
        return (isnothing(filter) || filter(node)) ? [node] : []
    end

    l = flattengptree(node.left, filter)
    r = isnothing(node.right) ? [] : flattengptree(node.right, filter)
    self = (isnothing(filter) || filter(node.func)) ? [node.func] : []

    return vcat(self, vcat(l, r))
end
countgptree(node, filter = nothing) =
    length(flattengptree(node, filter))

function subgptree(node, count_ref::Ref)
    count_ref[] -= 1

    if count_ref[] <= 0
        return node
    elseif !(node isa GPTree)
        return nothing
    end

    l = subgptree(node.left, count_ref)
    r = isnothing(node.right) ? nothing : subgptree(node.right, count_ref)

    return isnothing(l) ? (isnothing(r) ? nothing : r) : l
end
subgptree(node, at_idx::Integer) =
    subgptree(node, Ref(at_idx))

function insertgptree(node_target, node_donor, count_ref::Ref)
    if countgptree(node_target) < countgptree(node_donor)
        return node_target
    end

    count_ref[] -= 1
    
    if count_ref[] <= 0
        return count_ref[] == 0 ? node_donor : node_target
    elseif !(node_target isa GPTree)
        return node_target
    end

    l = insertgptree(node_target.left, node_donor, count_ref)
    r = isnothing(node_target.right) ? nothing : insertgptree(node_target.right, node_donor, count_ref)

    return GPTree(node_target.func, l, r)
end
insertgptree(node_target, node_donor, at_idx::Integer) =
    insertgptree(node_target, node_donor, Ref(at_idx))

function crossgptrees(node_target, node_donor, crossover_rate::AbstractFloat, max_size::Integer)
    if rand() >= crossover_rate
        return node_target
    end

    cut_at = rand(1:countgptree(node_donor))
    sub_donor = subgptree(node_donor, cut_at)
    max_index = (max_size - countgptree(sub_donor) + 1) ÷ 2
    insert_at = max_index > 1 ? rand(1:max_index) : 1

    return insertgptree(node_target, sub_donor, insert_at)
end
crossgptrees(node_target, node_donor, hyper::GPHyperparameters) =
    crossgptrees(node_target, node_donor, hyper.crossover_rate, hyper.max_size)


function errorgptree(node, data_vec::Vector, params::Vector)
    sum = 0.0

    for data = data_vec
        target = data[:target]
        value = evalgptree(node, data, params)
        sum += (target - value)^2
    end

    return sum / length(data_vec)
end

param_penalty(node, param_penalty_weight::AbstractFloat) =
    param_penalty_weight * countgptree(node, (x -> x isa OptimParam))
param_penalty(node, hyper::GPHyperparameters) =
    param_penalty(node, hyper.param_penalty_weight)

function fitnessgptree!(node, datasets::Vector, aggregation_func::Function, param_penalty_weight::AbstractFloat)::AbstractFloat
    errorfunc_factory = (data_vec) -> function(xs::Vector, grad::Vector)
        f(params) = errorgptree(node, data_vec, params)
        if isempty(grad)
            f(xs)
        else
            v, (g,) = AD.value_and_gradient(AD.ForwardDiffBackend(), f, xs)
            copy!(grad, g)
            v
        end
    end
    errorfuncs = [ errorfunc_factory(data_vec) for data_vec = datasets ]
    params = flattengptree(node, (x -> x isa OptimParam))
    errors = []
    for (dataset_idx, errorfunc) = enumerate(errorfuncs)
        param_values = [ get(param.val_vec_ref[], dataset_idx, rand()*2 - 1) for param = params ]
        param_count = length(param_values)
        if param_count > 0
            opt = Opt(:LD_LBFGS, param_count)
            opt.maxeval = 100
            opt.min_objective = errorfunc
            dataset_error =
                try
                    (result, new_values, return_code) = optimize(opt, param_values)
                    for param_idx = eachindex(params)
                        val_vec_ref = params[param_idx].val_vec_ref
                        if dataset_idx > length(val_vec_ref[])
                            push!(val_vec_ref[], new_values[param_idx])
                        else
                            val_vec_ref[][dataset_idx] = new_values[param_idx]
                        end
                    end
                    result
                catch e
                    if e isa ErrorException && occursin("nlopt", e.msg)
                        NaN
                    else
                        throw(e)
                    end
                end
            push!(errors, dataset_error)
        else
            dataset_error = errorfunc([], [])
            push!(errors, dataset_error)
        end
    end
    error = aggregation_func(errors)
    if isnan(error) return 0.0 end
    return 1 / (1 + error + param_penalty(node, param_penalty_weight))
end
fitnessgptree!(node, datasets::Vector, hyper::GPHyperparameters) =
    fitnessgptree!(node, datasets, hyper.aggregation_func, hyper.param_penalty_weight)

function rsquaredgptree_dataset(node, data_vec::Vector, param_idx::Integer)
    count = 0
    y_sum = 0.0
    for data = data_vec
        count += 1
        y_sum += data[:target]
    end
    y_mean = y_sum / count

    ss_res = 0.0
    ss_tot = 0.0
    params = flattengptree(node, (x -> x isa OptimParam))
    param_values = [ get(param.val_vec_ref[], param_idx, rand()*2 - 1) for param = params ]
    for data = data_vec
        yi = data[:target]
        fi = evalgptree(node, data, param_values)
        ss_res += (yi - fi)^2
        ss_tot += (yi - y_mean)^2
    end

    fvu = ss_res / ss_tot
    return 1 - fvu
end

function rsquaredgptree(node, datasets::Vector)
    r2s = [ rsquaredgptree_dataset(node, data_vec, dataset_idx) for (dataset_idx, data_vec) = enumerate(datasets) ]
    return sum(r2s) / length(r2s)
end

function pairexpressionlosses_population(pop::Vector, hyper::GPHyperparameters, datasets::Vector)
    mvsr_result = []
    for node = pop
        params = flattengptree(node, (x -> x isa OptimParam))
        expr = "print-expression" in ARGS ? string_gptree_pretty(node, hyper) : string_gptree(node)
        line = (expression=expr, losses=Vector{Float64}())
        for (dataset_idx, data_vec) = enumerate(datasets)
            param_values = [ get(param.val_vec_ref[], dataset_idx, rand()*2 - 1) for param = params ]
            push!(line.losses, errorgptree(node, data_vec, param_values))
        end
        push!(mvsr_result, line)
    end
    return mvsr_result
end

function tournamentgptrees(nodes::Vector, tournament_size::Integer, nodes_fitness::Vector)
    combatants_idxs = [ rand(1:length(nodes)) for _ = 1:tournament_size ]
    combatants = [ nodes[idx] for idx = combatants_idxs ]
    fitness = [ nodes_fitness[idx] for idx = combatants_idxs ]
    _, fittest_idx = findmax(fitness)
    return combatants[fittest_idx]
end
tournamentgptrees(nodes::Vector, hyper::GPHyperparameters, nodes_fitness::Vector) =
    tournamentgptrees(nodes, hyper.tournament_size, nodes_fitness)

function randomgptree_population(binary_functions::Vector, unary_functions::Vector,
                                 terminals::Vector, min_depth::Integer, max_depth::Integer,
                                 pop_size::Integer, max_size::Integer, datasets::Vector,
                                 aggregation_func::Function, param_penalty_weight::AbstractFloat)::Tuple
    population_and_fitness = Vector()

    ramp_start = 3
    per_iteration = pop_size / (max_depth - ramp_start + 1)
    per_mode = ceil(Integer, per_iteration / 2)
    for ramped_max_depth = ramp_start:max_depth
        for _ = 1:per_mode
            while true
                individual = randomgptree(binary_functions, unary_functions, terminals, min_depth, ramped_max_depth, max_size, 1, true)
                fitness = fitnessgptree!(individual, datasets, aggregation_func, param_penalty_weight)
                fitness > 0.0 || continue
                push!(population_and_fitness, (individual, fitness))
                break
            end
        end
        for _ = 1:per_mode
            while true
                individual = randomgptree(binary_functions, unary_functions, terminals, min_depth, ramped_max_depth, max_size, 1, false)
                fitness = fitnessgptree!(individual, datasets, aggregation_func, param_penalty_weight)
                fitness > 0.0 || continue
                push!(population_and_fitness, (individual, fitness))
                break
            end
        end
    end
    while length(population_and_fitness) > pop_size
        deleteat!(population_and_fitness, rand(1:length(population_and_fitness)))
    end

    return (Any[ pair[1] for pair = population_and_fitness ], [ pair[2] for pair = population_and_fitness ])
end
randomgptree_population(hyper::GPHyperparameters, datasets::Vector)::Tuple =
    randomgptree_population(hyper.binary_functions, hyper.unary_functions, hyper.terminals, hyper.min_depth, hyper.max_depth, hyper.pop_size, hyper.max_size, datasets, hyper.aggregation_func, hyper.param_penalty_weight)

using CSV, DataFrames

function generatedataset(df::DataFrame)::Vector
    data_vec = Vector()

    renamed_df = "target" in names(df) ? df : rename(df, [ncol(df) => :target])
    for row = eachrow(renamed_df)
        kv = zip([Symbol(name) for name = names(renamed_df)], row)
        push!(data_vec, Dict(kv))
    end

    return data_vec
end

function splitdataset(data_vec::Vector, parts::Integer)::Vector
    res = []
    data_vec_split = length(data_vec) ÷ parts
    if parts > 0
        for n = 0:parts-2
            push!(res, data_vec[1+n*data_vec_split:(n+1)*data_vec_split])
        end
        push!(res, data_vec[1+(parts-1)*data_vec_split:length(data_vec)])
    end
    return res
end

import AbstractDifferentiation as AD
using NLopt, ForwardDiff

# Main procedure - Evolutionary algorithm

if "print-expression" in ARGS
    using SymbolicUtils
end

if "generate-plot" in ARGS
    using PlotlyJS
end

function main(csv_file_or_folder::String, detailed_result = false,
              max_size = GPHyperparameters().max_size,
              extra_unary_functions = [], extra_binary_functions = [])
    abs_csv_file_or_folder = abspath(csv_file_or_folder)
    csv_files = isfile(abs_csv_file_or_folder) ?
        [ abs_csv_file_or_folder ] :
        [ file for file = readdir(abs_csv_file_or_folder, join=true) ]
    dfs = [ CSV.read(csv_file, DataFrame) for csv_file = csv_files ]
    hyper = GPHyperparameters()
    hyper.terminals = vcat([OptimParam], begin
        syms = names(dfs[1])
        if "target" in syms
            syms = filter(x -> x != "target", syms)
        else
            pop!(syms)
        end
        [Symbol(s) for s = syms]
    end)
    hyper.max_size = max_size
    hyper.unary_functions = vcat(hyper.unary_functions, extra_unary_functions)
    hyper.binary_functions = vcat(hyper.binary_functions, extra_binary_functions)

    datasets = []
    for df = dfs
        parts = 1
        data_vec = generatedataset(parts > 1 ? sort(df) : df)
        datasets = vcat(datasets, splitdataset(data_vec, parts))
    end

    pop, pop_fitness = randomgptree_population(hyper, datasets)
    best_of_run = pop[1]
    best_of_run_score = -Inf
    y_score = zeros(hyper.generations)
    y_size = zeros(hyper.generations)
    for gen = 1:hyper.generations
        fittest_score, fittest_idx = findmax(pop_fitness)
        mean_score = sum(pop_fitness) / length(pop_fitness)
        mean_size = sum([ countgptree(node) for node = pop ]) / length(pop)
        fittest_error = 1/fittest_score - param_penalty(pop[fittest_idx], hyper) - 1
        y_score[gen] = fittest_error
        y_size[gen] = mean_size
        if best_of_run_score <= fittest_score
            if best_of_run_score < fittest_score || countgptree(pop[fittest_idx]) < countgptree(best_of_run)
                best_of_run_score = fittest_score
                best_of_run = pop[fittest_idx]
                if !("quiet" in ARGS)
                    println("\nGENERATION ", gen)
                    println("Score (mean): ", mean_score)
                    println("Size  (mean): ", mean_size)
                    println("Score (best of run): ", fittest_score)
                    println("Size  (best of run): ", countgptree(pop[fittest_idx]))
                    println("Individual: ", string_gptree(pop[fittest_idx]))
                    if "print-expression" in ARGS
                        println("Expression: ", string_gptree_pretty(pop[fittest_idx], hyper))
                    end
                end
            end
        end
        offspring = Vector(undef, hyper.pool_size)
        offspring_idxs = [ rand(2:hyper.pop_size) for _ = 1:hyper.pool_size ]
        for n = 1:hyper.pool_size
            parent1 = deepcopy(tournamentgptrees(pop, hyper, pop_fitness))
            parent2 = deepcopy(tournamentgptrees(pop, hyper, pop_fitness))
            child = crossgptrees(parent1, parent2, hyper)
            at_idx = rand(1:countgptree(child))
            child = mutategptree(child, hyper, at_idx)
            offspring[n] = child
        end
        for (idx, offspring_idx) = enumerate(offspring_idxs)
            pop[offspring_idx] = offspring[idx]
        end
        pop[1] = deepcopy(best_of_run)
        for idx = offspring_idxs
            pop_fitness[idx] = fitnessgptree!(pop[idx], datasets, hyper)
        end
    end

    if "quiet" in ARGS
        println(best_of_run_score)
        if "print-expression" in ARGS
            println(string_gptree_pretty(best_of_run, hyper))
        end
    end

    if "generate-plot" in ARGS
        trace1 = scatter(x=1:hyper.generations, y=y_score, mode="lines")
        layout1 = Layout(
            title="Lowest error",
            xaxis_title="Generation",
            yaxis_title="Error",
            yaxis=attr(range=(0.0, 1.0), constrain="domain"),
            xaxis=attr(range=(1.0, Float64(hyper.generations)), constrain="domain")
        )
        p1 = plot(trace1, layout1)

        trace2 = scatter(x=1:hyper.generations, y=y_size, mode="lines")
        layout2 = Layout(
            title="Mean tree size",
            xaxis_title="Generation",
            yaxis_title="Size",
            yaxis=attr(range=(0.0, maximum(y_size)), constrain="domain"),
            xaxis=attr(range=(1.0, Float64(hyper.generations)), constrain="domain")
        )
        p2 = plot(trace2, layout2)

        p = [p1 ;; p2]
        relayout!(p, showlegend=false, title_text="Without bloat control")
        savefig(p, "plot.html")
    end

    if !detailed_result
        r_squared = rsquaredgptree(best_of_run, datasets)
        println(r_squared)
        return r_squared
    else
        return pairexpressionlosses_population([best_of_run], hyper, datasets)
    end
end

if abspath(PROGRAM_FILE) == @__FILE__
    main(last(ARGS))
end
