# SPDX-FileCopyrightText: 2019 Moshe Sipper, www.moshesipper.com
# SPDX-FileCopyrightText: 2024 Guilherme Leoi
#
# SPDX-License-Identifier: GPL-3.0-only

# Changes related to tiny_gp:
# - Implementation of various functions without side-effects
# - Mutation can occur on both branches, not only on left branches
# - Terminals are stored as values, not as childless tree nodes
# - Usage of PlotlyJS for plotting
# - R² measurement

mutable struct GPHyperparameters
    functions # Available functions for an expression tree (default: [+, -, *])
    terminals # Available leaf nodes for an expression tree (default: [:x, -2, -1, 0, 1, 2])
    pop_size # Number of individuals in the population (default: 60)
    min_depth # Minimum depth for an expression tree (default: 2)
    max_depth # Maximum depth for an expression tree (default: 5)
    tournament_size # Number of combatants in each round (default: 5)
    crossover_rate # Per-tree rate for crossover (default: 0.8)
    mutation_rate # Per-node rate for mutation (default: 0.2)
    generations # Number of iterations over the population (default: 250)
    bloat_control # Whether to enable bloat control or not (default: false)

    GPHyperparameters() = new([+, -, *], [:x, -2, -1, 0, 1, 2], 60, 2, 5, 5, 0.8, 0.2, 250, false)
end

struct GPTree
    func
    left
    right
end

function string_gptree(node)::String
    if !(node isa GPTree)
        return string(node)
    end
    l = string_gptree(node.left)
    r = string_gptree(node.right)
    return string('(', l, ' ', node.func, ' ', r, ')')
end

function evalgptree(node, data::Dict)
    if node isa GPTree
        try
            l = evalgptree(node.left, data)
            r = evalgptree(node.right, data)
            return node.func(l, r)
        catch e
            if e isa DomainError || e isa DivideError
                return NaN
            else
                throw(e)
            end
        end
    elseif node in keys(data)
        return data[node]
    end

    return node
end

function randomgptree(functions::Vector{Function}, terminals::Vector,
                      min_depth::Integer, max_depth::Integer,
                      grow::Bool = true, depth::Integer = 0)
    g = if depth < min_depth || (depth < max_depth && !grow)
        rand(functions)
    elseif depth >= max_depth
        rand(terminals)
    else
        rand() < 0.5 ? rand(functions) : rand(terminals)
    end

    if !(g isa Function)
        return g
    end

    l = randomgptree(functions, terminals, min_depth, max_depth, grow, depth + 1)
    r = randomgptree(functions, terminals, min_depth, max_depth, grow, depth + 1)

    return GPTree(g, l, r)
end
randomgptree(hyper::GPHyperparameters, grow::Bool = true, depth::Integer = 0) =
    randomgptree(hyper.functions, hyper.terminals, hyper.min_depth, hyper.max_depth, grow, depth)

function mutategptree(node, functions::Vector{Function}, terminals::Vector,
                      min_depth::Integer, mutation_rate::AbstractFloat)
    if rand() < mutation_rate
        return randomgptree(functions, terminals, min_depth, 2, true)
    elseif !(node isa GPTree)
        return node
    end

    l = node.left
    r = node.right
    if rand() < 0.5
        l = mutategptree(node.left, functions, terminals, min_depth, mutation_rate)
    else
        r = mutategptree(node.right, functions, terminals, min_depth, mutation_rate)
    end

    return GPTree(node.func, l, r)
end
mutategptree(node, hyper::GPHyperparameters) =
    mutategptree(node, hyper.functions, hyper.terminals, hyper.min_depth, hyper.mutation_rate)

function size_gptree(node)::Integer
    if !(node isa GPTree)
        return 1    
    end

    l = size_gptree(node.left)
    r = size_gptree(node.right)

    return 1 + l + r
end

function subgptree(node, count_ref::Ref)
    count_ref[] -= 1

    if count_ref[] <= 0
        return node
    elseif !(node isa GPTree)
        return nothing
    end

    l = subgptree(node.left, count_ref)
    r = subgptree(node.right, count_ref)

    return isnothing(l) ? (isnothing(r) ? nothing : r) : l
end
subgptree(node, at_idx::Integer) =
    subgptree(node, Ref(at_idx))

function insertgptree(node_target, node_donor, count_ref::Ref)
    count_ref[] -= 1
    
    if count_ref[] <= 0
        return count_ref[] == 0 ? node_donor : node_target
    elseif !(node_target isa GPTree)
        return node_target
    end

    l = insertgptree(node_target.left, node_donor, count_ref)
    r = insertgptree(node_target.right, node_donor, count_ref)

    return GPTree(node_target.func, l, r)
end
insertgptree(node_target, node_donor, at_idx::Integer) =
    insertgptree(node_target, node_donor, Ref(at_idx))

function crossgptrees(node_target, node_donor, crossover_rate::AbstractFloat)
    if rand() >= crossover_rate
        return node_target
    end

    cut_at = rand(1:size_gptree(node_donor))
    insert_at = rand(1:size_gptree(node_target))
    sub_donor = subgptree(node_donor, cut_at)

    return insertgptree(node_target, sub_donor, insert_at)
end
crossgptrees(node_target, node_donor, hyper::GPHyperparameters) =
    crossgptrees(node_target, node_donor, hyper.crossover_rate)


function errorgptree(node, data_vec::Vector)::AbstractFloat
    sum = 0.0

    for data = data_vec
        target = data[:target]
        value = evalgptree(node, data)
        sum += abs(target - value)
    end

    return sum / length(data_vec)
end

function fitnessgptree(node, data_vec::Vector, bloat_control::Bool)::AbstractFloat
    error = errorgptree(node, data_vec)
    if isnan(error) return 0.0 end
    bloatness = bloat_control ? 1e-2 * size_gptree(node) : 0
    return 1 / (1 + error + bloatness)
end
fitnessgptree(node, data_vec::Vector, hyper::GPHyperparameters)::AbstractFloat =
    fitnessgptree(node, data_vec, hyper.bloat_control)

function rsquaredgptree(node, data_vec::Vector)
    count = 0
    y_sum = 0.0
    for data = data_vec
        count += 1
        y_sum += data[:target]
    end
    y_mean = y_sum / count

    ss_res = 0.0
    ss_tot = 0.0
    for data = data_vec
        yi = data[:target]
        fi = evalgptree(node, data)
        ss_res += (yi - fi)^2
        ss_tot += (yi - y_mean)^2
    end

    fvu = ss_res / ss_tot
    return 1 - fvu
end

function tournamentgptrees(nodes::Vector, tournament_size::Integer, nodes_fitness::Vector)
    combatants_idxs = [ rand(1:length(nodes)) for _ = 1:tournament_size ]
    combatants = [ nodes[idx] for idx = combatants_idxs ]
    fitness = [ nodes_fitness[idx] for idx = combatants_idxs ]
    _, fittest_idx = findmax(fitness)
    return combatants[fittest_idx]
end
tournamentgptrees(nodes::Vector, hyper::GPHyperparameters, nodes_fitness::Vector) =
    tournamentgptrees(nodes, hyper.tournament_size, nodes_fitness)

function randomgptree_population(functions::Vector{Function}, terminals::Vector,
                                 min_depth::Integer, max_depth::Integer,
                                 pop_size::Integer)::Vector
    population = Vector()

    ramp_start = 3
    per_iteration = pop_size / (max_depth - ramp_start + 1)
    per_mode = ceil(Integer, per_iteration / 2)
    for ramped_max_depth = ramp_start:max_depth
        for _ = 1:per_mode
            push!(population, randomgptree(functions, terminals, min_depth, ramped_max_depth, true))
        end
        for _ = 1:per_mode
            push!(population, randomgptree(functions, terminals, min_depth, ramped_max_depth, false))
        end
    end
    while length(population) > pop_size
        deleteat!(population, rand(1:length(population)))
    end

    return population
end
randomgptree_population(hyper::GPHyperparameters)::Vector =
    randomgptree_population(hyper.functions, hyper.terminals, hyper.min_depth, hyper.max_depth, hyper.pop_size)

function generatedataset(target::Function)::Vector
    data_vec = Vector()

    for n = -100:2:100
        x = n / 100
        push!(data_vec, Dict(:x => x, :target => target(x)))
    end

    return data_vec
end

# Main procedure - Evolutionary algorithm

if "generate-plot" in ARGS
    using PlotlyJS
end

function main(targetfunction::Function)
    hyper = GPHyperparameters()

    data_vec = generatedataset(targetfunction)

    pop = randomgptree_population(hyper)
    best_of_run = pop[1]
    best_of_run_score = -Inf
    y_score = []
    y_size = []
    for gen = 1:hyper.generations
        pop_fitness = [ fitnessgptree(node, data_vec, hyper) for node = pop ]
        fittest_score, fittest_idx = findmax(pop_fitness)
        mean_score = sum(pop_fitness) / length(pop_fitness)
        mean_size = sum([ size_gptree(node) for node = pop ]) / length(pop)
        fittest_error = errorgptree(pop[fittest_idx], data_vec)
        push!(y_score, fittest_error)
        push!(y_size, mean_size)
        if best_of_run_score <= fittest_score
            if best_of_run_score < fittest_score || size_gptree(pop[fittest_idx]) < size_gptree(best_of_run)
                best_of_run_score = fittest_score
                best_of_run = pop[fittest_idx]
                if !("quiet" in ARGS)
                    println("\nGENERATION ", gen)
                    println("Score (mean): ", mean_score)
                    println("Size  (mean): ", mean_size)
                    println("Score (best of run): ", fittest_score)
                    println("Size  (best of run): ", size_gptree(pop[fittest_idx]))
                    println("Individual: ", string_gptree(pop[fittest_idx]))
                end
            end
        end
        new_pop = Vector()
        for n = 1:hyper.pop_size
            parent1 = tournamentgptrees(pop, hyper, pop_fitness)
            parent2 = tournamentgptrees(pop, hyper, pop_fitness)
            child = crossgptrees(parent1, parent2, hyper)
            child = mutategptree(child, hyper)
            push!(new_pop, child)
        end
        pop = new_pop
    end

    if "quiet" in ARGS
        println(best_of_run_score)
    end

    if "generate-plot" in ARGS
        trace1 = scatter(x=1:hyper.generations, y=y_score, mode="lines")
        layout1 = Layout(
            title="Lowest error",
            xaxis_title="Generation",
            yaxis_title="Error",
            yaxis=attr(range=(0.0, 1.0), constrain="domain"),
            xaxis=attr(range=(1.0, Float64(hyper.generations)), constrain="domain")
        )
        p1 = plot(trace1, layout1)

        trace2 = scatter(x=1:hyper.generations, y=y_size, mode="lines")
        layout2 = Layout(
            title="Mean tree size",
            xaxis_title="Generation",
            yaxis_title="Size",
            yaxis=attr(range=(0.0, maximum(y_size)), constrain="domain"),
            xaxis=attr(range=(1.0, Float64(hyper.generations)), constrain="domain")
        )
        p2 = plot(trace2, layout2)

        p = [p1 ;; p2]
        relayout!(p, showlegend=false, title_text="Without bloat control")
        savefig(p, "plot.html")
    end

    r_squared = rsquaredgptree(best_of_run, data_vec)
    println(r_squared)
    return r_squared
end

if abspath(PROGRAM_FILE) == @__FILE__
    main((x) -> x^4 + x^3 + x^2 + x + 1)
end