# SPDX-FileCopyrightText: 2019 Moshe Sipper, www.moshesipper.com
# SPDX-FileCopyrightText: 2024 Fabrício Olivetti de França, folivetti.github.io
# SPDX-FileCopyrightText: 2024 Guilherme Leoi
#
# SPDX-License-Identifier: GPL-3.0-only

# Changes related to tiny_gp:
# - Implementation of various functions without side-effects
# - Mutation can occur on both branches, not only on left branches
# - Terminals are stored as values, not as childless tree nodes
# - Usage of PlotlyJS for plotting
# - Support for unary functions
# - Dataset reading from CSV file
# - Conversion from tree to simplified expression
# - Parameter optimization
# - Tree size hard-limit as bloat control
# - R² measurement
# - All individuals from the first generation are valid

struct OptimParam
    val_ref::Ref

    OptimParam() = new(Ref(rand()*2 - 1))
end

pow_abs(x, y) = @fastmath (abs(x)^y)

mutable struct GPHyperparameters
    binary_functions # Available two-parameter functions for an expression tree (default: [+, -, *, /, pow])
    unary_functions # Available one-parameter functions for an expression tree (default: [inv])
    terminals # Available leaf nodes for an expression tree (default: [:x, OptimParam])
    pop_size # Number of individuals in the population (default: 200)
    min_depth # Minimum depth for an expression tree (default: 2)
    max_depth # Maximum depth for an expression tree (default: 4)
    tournament_size # Number of combatants in each round (default: 2)
    crossover_rate # Per-tree rate for crossover (default: 1.0)
    mutation_rate # Per-tree rate for mutation (default: 0.25)
    generations # Number of iterations over the population (default: 500)
    max_size # Maximum node count for an expression tree, acts as bloat control (default: 10)

    GPHyperparameters() = new([+, -, *, /, pow_abs], [inv], [:x, OptimParam], 200, 2, 4, 2, 1.0, 0.25, 500, 10)
end

struct GPTree
    func
    left
    right
end

function string_gptree(node)::String
    if !(node isa GPTree)
        return node isa OptimParam ? string("param{", node.val_ref[] ,'}') : string(node)
    end
    l = string_gptree(node.left)
    if (isnothing(node.right))
        return string('(', node.func, ' ', l, ')')
    else
        r = string_gptree(node.right)
        return string('(', l, ' ', node.func, ' ', r, ')')
    end
end

function string_gptree_pretty(node, terminals::Vector)::String
    symbols = filter(item -> item isa Symbol && item != :p, terminals)
    symbols_params = [Symbol("p", idx) for idx = 1:countgptree(node, (x) -> x isa OptimParam)]
    u = eval(:(@syms $(symbols...)))
    u_params = eval(:(@syms $(symbols_params...)))
    symbols_to_u = Dict([(symbols[idx], u[idx]) for idx = eachindex(symbols)])
    expr = evalgptree(node, symbols_to_u, [pn for pn = u_params])
    try
        expr_simplified = simplify(expr, expand=true)
        return string(expr_simplified)
    finally
        return string(expr)
    end
end
string_gptree_pretty(node, hyper::GPHyperparameters)::String =
    string_gptree_pretty(node, hyper.terminals)

function evalgptree(node, data::Dict, params::Vector, param_idx_ref::Ref)
    if node isa GPTree
        try
            l = evalgptree(node.left, data, params, param_idx_ref)
            if (isnothing(node.right))
                return node.func(l)
            else
                r = evalgptree(node.right, data, params, param_idx_ref) 
                return node.func(l, r)
            end
        catch e
            if e isa DomainError || e isa DivideError
                return NaN
            else
                throw(e)
            end
        end
    elseif node isa OptimParam
        param = params[param_idx_ref[]]
        param_idx_ref[] += 1
        return param
    elseif node in keys(data)
        return data[node]
    end

    return node
end
evalgptree(node, data::Dict, params::Vector) =
    evalgptree(node, data, params, Ref(1))

function randomgptree(binary_functions::Vector, unary_functions::Vector,
                      terminals::Vector, min_depth::Integer, max_depth::Integer,
                      max_size::Integer, count::Integer, grow::Bool = true, depth::Integer = 0)
    functions = vcat(binary_functions, unary_functions)
    max_arity = (isempty(binary_functions) ? 1 : 2)
    pre_last_leaves = max_size - max_arity
    g = if (depth < min_depth || (depth < max_depth && !grow)) && count <= pre_last_leaves
        rand(functions)
    elseif depth >= max_depth || count > pre_last_leaves
        rand(terminals)
    else
        rand() < 0.5 ? rand(functions) : rand(terminals)
    end

    if !(g isa Function)
        return g isa Type ? g() : g
    end

    g_extra = g in unary_functions ? 0 : 1
    l = randomgptree(binary_functions, unary_functions, terminals, min_depth, max_depth, max_size, count + g_extra + 1, grow, depth + 1)
    r = nothing
    if g_extra > 0
        l_count = countgptree(l)
        r = randomgptree(binary_functions, unary_functions, terminals, min_depth, max_depth, max_size, count + l_count + 1, grow, depth + 1)
    end

    return GPTree(g, l, r)
end
randomgptree(hyper::GPHyperparameters, grow::Bool = true, depth::Integer = 0) =
    randomgptree(hyper.binary_functions, hyper.unary_functions, hyper.terminals, hyper.min_depth, hyper.max_depth, hyper.max_size, 1, grow, depth)

function mutategptree(node, binary_functions::Vector, unary_functions::Vector,
                      terminals::Vector, min_depth::Integer, mutation_rate::AbstractFloat,
                      max_size::Integer, depth::Integer, index_ref::Ref, tree_size::Integer = countgptree(node))
    if rand() < mutation_rate || depth > 0
        index_ref[] -= 1

        if index_ref[] == 0
            adapted_max_size = max_size - tree_size + countgptree(node)
            return randomgptree(binary_functions, unary_functions, terminals, min_depth, 2, adapted_max_size, 1, true, depth)
        elseif !(node isa GPTree) || index_ref[] < 0
            return node
        end

        l = mutategptree(node.left, binary_functions, unary_functions, terminals, min_depth, mutation_rate, max_size, depth + 1, index_ref, tree_size)
        r = isnothing(node.right) ?
            nothing :
            mutategptree(node.right, binary_functions, unary_functions, terminals, min_depth, mutation_rate, max_size, depth + 1, index_ref, tree_size)

        return GPTree(node.func, l, r)
    end

    return node
end
mutategptree(node, hyper::GPHyperparameters, at_idx::Integer) =
    mutategptree(node, hyper.binary_functions, hyper.unary_functions, hyper.terminals, hyper.min_depth, hyper.mutation_rate, hyper.max_size, 0, Ref(at_idx))

function flattengptree(node, filter = nothing)
    if !(node isa GPTree)
        return (isnothing(filter) || filter(node)) ? [node] : []
    end

    l = flattengptree(node.left, filter)
    r = isnothing(node.right) ? [] : flattengptree(node.right, filter)
    self = (isnothing(filter) || filter(node.func)) ? [node.func] : []

    return vcat(self, vcat(l, r))
end
countgptree(node, filter = nothing) =
    length(flattengptree(node, filter))

function subgptree(node, count_ref::Ref)
    count_ref[] -= 1

    if count_ref[] <= 0
        return node
    elseif !(node isa GPTree)
        return nothing
    end

    l = subgptree(node.left, count_ref)
    r = isnothing(node.right) ? nothing : subgptree(node.right, count_ref)

    return isnothing(l) ? (isnothing(r) ? nothing : r) : l
end
subgptree(node, at_idx::Integer) =
    subgptree(node, Ref(at_idx))

function insertgptree(node_target, node_donor, count_ref::Ref)
    if countgptree(node_target) < countgptree(node_donor)
        return node_target
    end

    count_ref[] -= 1
    
    if count_ref[] <= 0
        return count_ref[] == 0 ? node_donor : node_target
    elseif !(node_target isa GPTree)
        return node_target
    end

    l = insertgptree(node_target.left, node_donor, count_ref)
    r = isnothing(node_target.right) ? nothing : insertgptree(node_target.right, node_donor, count_ref)

    return GPTree(node_target.func, l, r)
end
insertgptree(node_target, node_donor, at_idx::Integer) =
    insertgptree(node_target, node_donor, Ref(at_idx))

function crossgptrees(node_target, node_donor, crossover_rate::AbstractFloat, max_size::Integer)
    if rand() >= crossover_rate
        return node_target
    end

    cut_at = rand(1:countgptree(node_donor))
    sub_donor = subgptree(node_donor, cut_at)
    max_index = (max_size - countgptree(sub_donor) + 1) ÷ 2
    insert_at = max_index > 1 ? rand(1:max_index) : 1

    return insertgptree(node_target, sub_donor, insert_at)
end
crossgptrees(node_target, node_donor, hyper::GPHyperparameters) =
    crossgptrees(node_target, node_donor, hyper.crossover_rate, hyper.max_size)


function errorgptree(node, data_vec::Vector, params::Vector)
    sum = 0.0

    for data = data_vec
        target = data[:target]
        value = evalgptree(node, data, params)
        sum += (target - value)^2
    end

    return sum / length(data_vec)
end

function fitnessgptree!(node, data_vec::Vector)::AbstractFloat
    errorfunc = function(xs::Vector, grad::Vector)
        f(params) = errorgptree(node, data_vec, params)
        if isempty(grad)
            f(xs)
        else
            v, (g,) = AD.value_and_gradient(AD.ForwardDiffBackend(), f, xs)
            copy!(grad, g)
            v
        end
    end
    params = flattengptree(node, (x -> x isa OptimParam))
    param_values = [ param.val_ref[] for param = params ]
    param_count = length(param_values)
    if param_count > 0
        opt = Opt(:LD_LBFGS, param_count)
        opt.maxeval = 10
        opt.min_objective = errorfunc
        error =
            try
                (result, new_values, return_code) = optimize(opt, param_values)
                for idx = eachindex(params)
                    params[idx].val_ref[] = new_values[idx]
                end
                result
            catch e
                if e isa ErrorException && occursin("nlopt", e.msg)
                    NaN
                else
                    throw(e)
                end
            end
    else
        error = errorfunc([], [])
    end
    if isnan(error) return 0.0 end
    return 1 / (1 + error)
end

function rsquaredgptree(node, data_vec::Vector)
    count = 0
    y_sum = 0.0
    for data = data_vec
        count += 1
        y_sum += data[:target]
    end
    y_mean = y_sum / count

    ss_res = 0.0
    ss_tot = 0.0
    params = flattengptree(node, (x -> x isa OptimParam))
    param_values = [ param.val_ref[] for param = params ]
    for data = data_vec
        yi = data[:target]
        fi = evalgptree(node, data, param_values)
        ss_res += (yi - fi)^2
        ss_tot += (yi - y_mean)^2
    end

    fvu = ss_res / ss_tot
    return 1 - fvu
end

function tournamentgptrees(nodes::Vector, tournament_size::Integer, nodes_fitness::Vector)
    combatants_idxs = [ rand(1:length(nodes)) for _ = 1:tournament_size ]
    combatants = [ nodes[idx] for idx = combatants_idxs ]
    fitness = [ nodes_fitness[idx] for idx = combatants_idxs ]
    _, fittest_idx = findmax(fitness)
    return combatants[fittest_idx]
end
tournamentgptrees(nodes::Vector, hyper::GPHyperparameters, nodes_fitness::Vector) =
    tournamentgptrees(nodes, hyper.tournament_size, nodes_fitness)

function randomgptree_population(binary_functions::Vector, unary_functions::Vector,
                                 terminals::Vector, min_depth::Integer, max_depth::Integer,
                                 pop_size::Integer, max_size::Integer, data_vec::Vector)::Tuple
    population_and_fitness = Vector()

    ramp_start = 3
    per_iteration = pop_size / (max_depth - ramp_start + 1)
    per_mode = ceil(Integer, per_iteration / 2)
    for ramped_max_depth = ramp_start:max_depth
        for _ = 1:per_mode
            while true
                individual = randomgptree(binary_functions, unary_functions, terminals, min_depth, ramped_max_depth, max_size, 1, true)
                fitness = fitnessgptree!(individual, data_vec)
                fitness > 0.0 || continue
                push!(population_and_fitness, (individual, fitness))
                break
            end
        end
        for _ = 1:per_mode
            while true
                individual = randomgptree(binary_functions, unary_functions, terminals, min_depth, ramped_max_depth, max_size, 1, false)
                fitness = fitnessgptree!(individual, data_vec)
                fitness > 0.0 || continue
                push!(population_and_fitness, (individual, fitness))
                break
            end
        end
    end
    while length(population_and_fitness) > pop_size
        deleteat!(population_and_fitness, rand(1:length(population_and_fitness)))
    end

    return (Any[ pair[1] for pair = population_and_fitness ], [ pair[2] for pair = population_and_fitness ])
end
randomgptree_population(hyper::GPHyperparameters, data_vec::Vector)::Tuple =
    randomgptree_population(hyper.binary_functions, hyper.unary_functions, hyper.terminals, hyper.min_depth, hyper.max_depth, hyper.pop_size, hyper.max_size, data_vec)

using CSV, DataFrames

function generatedataset(df::DataFrame)::Vector
    data_vec = Vector()

    renamed_df = "target" in names(df) ? df : rename(df, [ncol(df) => :target])
    for row = eachrow(renamed_df)
        kv = zip([Symbol(name) for name = names(renamed_df)], row)
        push!(data_vec, Dict(kv))
    end

    return data_vec
end

import AbstractDifferentiation as AD
using NLopt, ForwardDiff

# Main procedure - Evolutionary algorithm

if "print-expression" in ARGS
    using SymbolicUtils
end

if "generate-plot" in ARGS
    using PlotlyJS
end

function main(csv_file::String)
    df = CSV.read(csv_file, DataFrame)
    hyper = GPHyperparameters()
    hyper.terminals = vcat([OptimParam], begin
        syms = names(df)
        if "target" in syms
            syms = filter(x -> x != "target", syms)
        else
            pop!(syms)
        end
        [Symbol(s) for s = syms]
    end)

    data_vec = generatedataset(df)

    pop, pop_fitness = randomgptree_population(hyper, data_vec)
    best_of_run = pop[1]
    best_of_run_score = -Inf
    y_score = []
    y_size = []
    for gen = 1:hyper.generations
        pop_fitness = [ fitnessgptree!(node, data_vec) for node = pop ]
        fittest_score, fittest_idx = findmax(pop_fitness)
        mean_score = sum(pop_fitness) / length(pop_fitness)
        mean_size = sum([ countgptree(node) for node = pop ]) / length(pop)
        fittest_error = 1/fittest_score - 1
        push!(y_score, fittest_error)
        push!(y_size, mean_size)
        if best_of_run_score <= fittest_score
            if best_of_run_score < fittest_score || countgptree(pop[fittest_idx]) < countgptree(best_of_run)
                best_of_run_score = fittest_score
                best_of_run = pop[fittest_idx]
                if !("quiet" in ARGS)
                    println("\nGENERATION ", gen)
                    println("Score (mean): ", mean_score)
                    println("Size  (mean): ", mean_size)
                    println("Score (best of run): ", fittest_score)
                    println("Size  (best of run): ", countgptree(pop[fittest_idx]))
                    println("Individual: ", string_gptree(pop[fittest_idx]))
                    if "print-expression" in ARGS
                        println("Expression: ", string_gptree_pretty(pop[fittest_idx], hyper))
                    end
                end
            end
        end
        new_pop = Vector()
        for n = 2:hyper.pop_size
            parent1 = deepcopy(tournamentgptrees(pop, hyper, pop_fitness))
            parent2 = deepcopy(tournamentgptrees(pop, hyper, pop_fitness))
            child = crossgptrees(parent1, parent2, hyper)
            at_idx = rand(1:countgptree(child))
            child = mutategptree(child, hyper, at_idx)
            push!(new_pop, child)
        end
        new_pop[1] = deepcopy(best_of_run)
        pop = new_pop
    end

    if "quiet" in ARGS
        println(best_of_run_score)
        if "print-expression" in ARGS
            println(string_gptree_pretty(best_of_run, hyper))
        end
    end

    if "generate-plot" in ARGS
        trace1 = scatter(x=1:hyper.generations, y=y_score, mode="lines")
        layout1 = Layout(
            title="Lowest error",
            xaxis_title="Generation",
            yaxis_title="Error",
            yaxis=attr(range=(0.0, 1.0), constrain="domain"),
            xaxis=attr(range=(1.0, Float64(hyper.generations)), constrain="domain")
        )
        p1 = plot(trace1, layout1)

        trace2 = scatter(x=1:hyper.generations, y=y_size, mode="lines")
        layout2 = Layout(
            title="Mean tree size",
            xaxis_title="Generation",
            yaxis_title="Size",
            yaxis=attr(range=(0.0, maximum(y_size)), constrain="domain"),
            xaxis=attr(range=(1.0, Float64(hyper.generations)), constrain="domain")
        )
        p2 = plot(trace2, layout2)

        p = [p1 ;; p2]
        relayout!(p, showlegend=false, title_text="Without bloat control")
        savefig(p, "plot.html")
    end

    r_squared = rsquaredgptree(best_of_run, data_vec)
    println(r_squared)
    return r_squared
end

if abspath(PROGRAM_FILE) == @__FILE__
    main(last(ARGS))
end
