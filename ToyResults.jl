# SPDX-FileCopyrightText: 2024 Guilherme Leoi
#
# SPDX-License-Identifier: GPL-3.0-only

using CSV, DataFrames

push!(ARGS, "quiet")
push!(ARGS, "print-expression")
include("MvSR.jl")

function run(channel, func, noise, max_size, data_folder, results_folder, optsin = false)
	@time begin
		suffix = "$func/$noise"
		path = "$(pwd())/$toy_data_folder/$suffix"
		df = DataFrame(main(path, true, max_size, optsin ? [sin] : []))
		csv_path = "$(pwd())/$toy_results_folder/$suffix/max$max_size"
		put!(channel, (path=csv_path, result=df))
	end
end

function write_results(channel)
    while isready(channel)
        msg = take!(channel)
        mkpath(msg.path)
        file = "$(msg.path)/MvSR_results.csv"
        CSV.write(file, msg.result; append=isfile(file))
    end
end

# =============================== PARAMETERS ================================ #

toy_data_folder = "toy_data"
toy_results_folder = "toy_results"

functions = [
    "friedman2",
    "polynomial_partial",
    "polynomial0",
]
functions_sin = [
    "friedman1",
]

noise_levels = [
    "noisy_0.1",
    "noisy_0.066",
    "noisy_0.033",
    "perfect",
]

max_sizes = [
    5,
    7,
    9,
    11,
    13,
    15,
    17,
    19,
    21,
    23,
    25,
]

retries = 28

# =========================================================================== #

tasks = Vector()
total = (length(functions) + length(functions_sin)) * length(noise_levels) * length(max_sizes) * retries
channel = Channel(total)

for func = functions
    for noise = noise_levels
        for max_size = max_sizes
            for _ = 1:retries
				push!(tasks, Threads.@spawn begin
					run(channel, func, noise, max_size, toy_data_folder, toy_results_folder)
				end)
			end
        end
    end
end

for func = functions_sin
    for noise = noise_levels
        for max_size = max_sizes
            for _ = 1:retries
				push!(tasks, Threads.@spawn begin
					run(channel, func, noise, max_size, toy_data_folder, toy_results_folder, true)
				end)
			end
		end
    end
end

while any([ !istaskdone(task) for task = tasks ])
	sleep(2)
	write_results(channel)
end
sleep(1)

write_results(channel)
sleep(5)
