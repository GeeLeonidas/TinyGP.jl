# SPDX-FileCopyrightText: 2024 Guilherme Leoi
#
# SPDX-License-Identifier: GPL-3.0-only

using Pkg

Pkg.add([
    (name="CSV", version="0.10.15"),
    (name="DataFrames", version="1.7.0"),
    
    (name="NLopt", version="1.1.1"),
    (name="ForwardDiff", version="0.10.36"),
    (name="AbstractDifferentiation", version="0.6.2"),
    
    (name="PlotlyJS", version="0.18.14"),
    (name="SymbolicUtils", version="3.5.0"),
])
