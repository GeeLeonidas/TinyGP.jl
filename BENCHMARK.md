<!--
SPDX-FileCopyrightText: 2024 Guilherme Leoi

SPDX-License-Identifier: CC-BY-4.0
-->

# Benchmark between `TinyGP.jl` and `tiny_gp.py`
*Data gathered by running `perf stat -r 20 <command>`*

## Without bloat control
### `tiny_gp.py`
```
 Performance counter stats for 'python tiny_gp.py' (20 runs):

        121,325.64 msec task-clock:u                     #    0.990 CPUs utilized               ( +- 32.12% )
                 0      context-switches:u               #    0.000 /sec                      
                 0      cpu-migrations:u                 #    0.000 /sec                      
         2,596,804      page-faults:u                    #   21.404 K/sec                       ( +- 36.25% )
   437,293,813,223      cycles:u                         #    3.604 GHz                         ( +- 31.35% )
     1,382,507,034      stalled-cycles-frontend:u        #    0.32% frontend cycles idle        ( +- 28.60% )
       515,903,466      stalled-cycles-backend:u         #    0.12% backend cycles idle         ( +- 31.80% )
   958,506,912,149      instructions:u                   #    2.19  insn per cycle            
                                                         #    0.00  stalled cycles per insn     ( +- 28.52% )
   178,240,712,600      branches:u                       #    1.469 G/sec                       ( +- 28.60% )
     1,228,261,399      branch-misses:u                  #    0.69% of all branches             ( +- 28.58% )

            122.49 +- 39.39 seconds time elapsed  ( +- 32.16% ) 
```
### `TinyGP.jl`
```
 Performance counter stats for 'julia TinyGP.jl' (20 runs):

         26,671.14 msec task-clock:u                     #    1.020 CPUs utilized               ( +- 34.84% )
                 0      context-switches:u               #    0.000 /sec                      
                 0      cpu-migrations:u                 #    0.000 /sec                      
            97,526      page-faults:u                    #    3.657 K/sec                       ( +- 22.72% )
   110,299,191,589      cycles:u                         #    4.136 GHz                         ( +- 35.54% )
       126,657,907      stalled-cycles-frontend:u        #    0.11% frontend cycles idle        ( +- 30.31% )
        84,993,344      stalled-cycles-backend:u         #    0.08% backend cycles idle         ( +-  7.35% )
   254,856,393,265      instructions:u                   #    2.31  insn per cycle            
                                                         #    0.00  stalled cycles per insn     ( +- 34.05% )
    47,821,887,757      branches:u                       #    1.793 G/sec                       ( +- 33.89% )
       772,520,974      branch-misses:u                  #    1.62% of all branches             ( +- 41.96% )

             26.15 +- 9.29 seconds time elapsed  ( +- 35.53% ) 
```
`TinyGP.jl` is $4.68 \pm 2.24$ times faster than `tiny_gp.py` when bloat control is disabled

## With bloat control
### `tiny_gp.py`
```
 Performance counter stats for 'python tiny_gp.py' (20 runs):

          4,263.68 msec task-clock:u                     #    0.999 CPUs utilized               ( +-  7.94% )
                 0      context-switches:u               #    0.000 /sec                      
                 0      cpu-migrations:u                 #    0.000 /sec                      
             1,584      page-faults:u                    #  371.510 /sec                        ( +-  0.51% )
    18,039,839,711      cycles:u                         #    4.231 GHz                         ( +-  7.95% )
        67,446,095      stalled-cycles-frontend:u        #    0.37% frontend cycles idle        ( +-  8.63% )
        18,717,952      stalled-cycles-backend:u         #    0.10% backend cycles idle         ( +-  9.24% )
    50,878,863,836      instructions:u                   #    2.82  insn per cycle            
                                                         #    0.00  stalled cycles per insn     ( +-  7.95% )
     9,420,602,602      branches:u                       #    2.210 G/sec                       ( +-  7.91% )
        54,474,179      branch-misses:u                  #    0.58% of all branches             ( +-  8.93% )

             4.267 +- 0.339 seconds time elapsed  ( +-  7.94% )
```
### `TinyGP.jl`
```
 Performance counter stats for 'julia TinyGP.jl' (20 runs):

          1,942.41 msec task-clock:u                     #    1.367 CPUs utilized               ( +-  1.61% )
                 0      context-switches:u               #    0.000 /sec                      
                 0      cpu-migrations:u                 #    0.000 /sec                      
            49,316      page-faults:u                    #   25.389 K/sec                       ( +-  1.52% )
     5,950,667,231      cycles:u                         #    3.064 GHz                         ( +-  2.17% )
         4,837,648      stalled-cycles-frontend:u        #    0.08% frontend cycles idle        ( +-  3.22% )
        74,185,453      stalled-cycles-backend:u         #    1.25% backend cycles idle         ( +-  7.15% )
     9,087,303,712      instructions:u                   #    1.53  insn per cycle            
                                                         #    0.01  stalled cycles per insn     ( +-  4.03% )
     1,873,470,365      branches:u                       #  964.508 M/sec                       ( +-  3.64% )
        56,362,740      branch-misses:u                  #    3.01% of all branches             ( +-  0.76% )

            1.4210 +- 0.0313 seconds time elapsed  ( +-  2.20% ) 
```
`TinyGP.jl` is $3.003 \pm 0.247$ times faster than `tiny_gp.py` when bloat control is enabled
