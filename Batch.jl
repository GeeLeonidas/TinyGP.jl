# SPDX-FileCopyrightText: 2024 Guilherme Leoi
#
# SPDX-License-Identifier: GPL-3.0-only

retries = 1
for arg = ARGS
    if startswith(arg, "retries")
        retries_number = parse(Int8, split(arg, ':')[2])
        @assert retries_number >= 1
        global retries = retries_number
    end
end

if "extra" in ARGS
    include("TinyGPExtra.jl")
    impl = () ->
        @eval main(last(ARGS))
elseif "mvsr" in ARGS
    include("MvSR.jl")
    impl = () ->
        @eval main(last(ARGS))
elseif "base" in ARGS
    include("TinyGP.jl")
    impl = () ->
        @eval main((x) -> x^4 + x^3 + x^2 + x + 1)
elseif "python" in ARGS
    cmd = addenv(`python3 $(last(ARGS))`, "PYTHONWARNINGS" => "ignore")
    impl = () ->
        parse(Float64, last(split(readchomp(cmd), '\n')))
elseif "fresh_base" in ARGS
    cmd = `julia TinyGP.jl quiet $(last(ARGS))`
    impl = () ->
        parse(Float64, last(split(readchomp(cmd), '\n')))
elseif "fresh_mvsr" in ARGS
    cmd = `julia MvSR.jl quiet $(last(ARGS))`
    impl = () ->
        parse(Float64, last(split(readchomp(cmd), '\n')))
elseif "fresh_extra" in ARGS
    cmd = `julia TinyGPExtra.jl quiet $(last(ARGS))`
    impl = () ->
        parse(Float64, last(split(readchomp(cmd), '\n')))
else
    @error "Either `extra`, `base` or `python` parameter is required in order to select an implementation"
end

r_squared_history = []
duration_history = []
for n = 1:retries
    stats = @timed impl()
    push!(r_squared_history, stats.value)
    push!(duration_history, stats.time)
end

using Statistics
println("Mean R²: $(sum(r_squared_history)/retries) ± $(std(r_squared_history))")
println("Mean duration: $(sum(duration_history)/retries) ± $(std(duration_history)) seconds")