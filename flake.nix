# SPDX-FileCopyrightText: 2024 Guilherme Leoi
#
# SPDX-License-Identifier: GPL-3.0-only

{
  description = "Julia Template";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # Get set of packages
        pkgs = import nixpkgs {
          inherit system;
        };
        # Add compile-time dependencies
        nativeBuildInputs = with pkgs; [
          # ...
        ];
        # Add run-time dependencies
        buildInputs = with pkgs; [
          julia-lts
        ] ++ (with pkgs.python311Packages; [
          python
          numpy
          pandas
          scipy
        ]);
      in {
        devShells.default = pkgs.mkShell {
          inherit nativeBuildInputs buildInputs;
          shellHook = ''
            export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH
            export PATH=${pkgs.lib.makeBinPath (nativeBuildInputs ++ buildInputs)}:$PATH
          '';
        };
      }
    );
}